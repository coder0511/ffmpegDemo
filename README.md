# ffmpegDemo

说明：

1.支持linux下编译学习使用，ffmpeg库使用的版本2.8.5

2.编译前需要安装一些基本库如zlib pthread等

3.编译命令注意库的各个依赖顺序关系

4.用到的插件x264 x265等放到ffmpeg静态库之后

5.文件编译时需要将文件利用dos2unix 转成unix形式
=========================================================================================================================================

本地编译命令

1：g++ simplest_ffmpeg_muxer.cpp -g -o testffmpeg -I ../../Include/ffmpeg -L../../Lib/m64x86/ffmpeg -lavformat -lavdevice -lavcodec -lavutil -lswscale -lavfilter -lpostproc -lswresample -lx264 -lx265 -lfaac -lm -ldl -lpthread -lrt -lz

2：g++ simplest_ffmpeg_demuxer.cpp -g -o testffmpegDemuxer -I ../../Include/ffmpeg -L../../Lib/m64x86/ffmpeg -lavformat -lavdevice -lavcodec -lavutil -lswscale -lavfilter -lpostproc -lswresample -lx264 -lx265 -lfaac -lm -ldl -lpthread -lrt -lz

3：g++ simplest_ffmpeg_demuxer_simple.cpp -g -o testffmpegDemuxer_simple -I ../../Include/ffmpeg -L../../Lib/m64x86/ffmpeg -lavformat -lavdevice -lavcodec -lavutil -lswscale -lavfilter -lpostproc -lswresample -lx264 -lx265 -lfaac -lm -ldl -lpthread -lrt -lz

4：g++ simplest_ffmpeg_remuxer.cpp -g -o testffmpegRemuxer -I ../../Include/ffmpeg -L../../Lib/m64x86/ffmpeg -lavformat -lavdevice -lavcodec -lavutil -lswscale -lavfilter -lpostproc -lswresample -lx264 -lx265 -lfaac -lm -ldl -lpthread -lrt -lz

[YUV420编码H.264]
5.g++ simplest_ffmpeg_video_encoder.cpp -g -o testffmpegEncoder_YUV420_H264 -I ../../Include/ffmpeg -L../../Lib/m64x86/ffmpeg -lavformat -lavdevice -lavcodec -lavutil -lswscale -lavfilter -lpostproc -lswresample -lx264 -lx265 -lfaac -lm -ldl -lpthread -lrt -lz