/*----------------------------------------------------------------------------------------------
*
* This file is ArcSoft's property. It contains ArcSoft's trade secret, proprietary and 		
* confidential information. 
* 
* The information and code contained in this file is only for authorized ArcSoft employees 
* to design, create, modify, or review.
* 
* DO NOT DISTRIBUTE, DO NOT DUPLICATE OR TRANSMIT IN ANY FORM WITHOUT PROPER AUTHORIZATION.
* 
* If you are not an intended recipient of this file, you must not copy, distribute, modify, 
* or take any action in reliance on it. 
* 
* If you have received this file in error, please immediately notify ArcSoft and 
* permanently delete the original and any copy of any file and any printout thereof.
*
*-------------------------------------------------------------------------------------------------*/
 
/*
 * G711Decoder.h
 *
 * Description:
 *
 *	The common used marco and structure  in MVLIB2.0
 *
 *
 * History
 *    
 *  04-08-2015 Haiyan Huang(hhy2603@arcsoft.com.cn )   
 * - initial version 
 *
 *
 */

#ifndef _G711_DECODER_LINUX_H_
#define _G711_DECODER_LINUX_H_

#define MV2_CODEC_TYPE_G711A      0
#define MV2_CODEC_TYPE_G711U      1

#define MV2_ERR_NONE    0x0000

typedef struct G711Context {
    unsigned long SampleRate;
} G711Context;

#ifdef USE_DENOISE
#include "speex_preprocess.h"
#endif

#define GLOBAL __attribute__((visibility("default")))
#define LOCAL  __attribute__((visibility("hidden")))

/*
 * init for decoder;
 *
 * Parameter:
 *        SampleRate:  [in]  SampleRate for audio.
 */
GLOBAL void* createG711(unsigned long SampleRate);

/*
 * Uninit for decoder;
 */
GLOBAL void  deleteG711(void* Handle);

/*	decodeG726Frame 
 * 
 *	Description: 
 *		Call this function to decode one encoded media data frame. 
 *		It is a memory buffer to memory buffer operation. 
 *
 *	Parameters: 
 *           audioType:           [in]               type of audio, the decoder support is MV2_CODEC_TYPE_G711A and MV2_CODEC_TYPE_G711U
 *		pIn : 			[in] 			Pointer to the buffer containing the media data to be decoded.
 *		lInSize: 		       [in] 			Number of bytes to decode
 *		plInSize		       [out]	       pointer to bytes that have been deocded of the pIn buffer
 *		pOut: 			[out] 		Pointer to the buffer that receives the data decode from the input buffer.
 *		lOutBufferSize:      [in] 			The byte size of the output buffer pointed by pOut. 
 *		plOutSize:		[out] 		Pointer to the number of bytes raw data that decodeframe() filled in the output buff. 
 *	
 *
 *	Return:
 *		MV2_ERR_NONE			success
 *		!MV2_ERR_NONE			fail
 */
GLOBAL long decodeG711Frame(void * Handle, int audioType, unsigned char* pIn,
                                  long   lInSize,
                                  long  *plInSize,
                                  short *pOut,
                                  long   lOutBufferSize,
                                  long  *plOutSize );

GLOBAL long G711ResamplePcm(void* Handle, unsigned long srcSampleRate, 
				           unsigned long dstSampleRate, 
                           unsigned char* pdstPcmBuf,
				           long ldstPcmBufSize,
				           unsigned char* psrcPcmBuf,
				           long lsrcPcmBufSize );



#endif // _G711_DECODER_LINUX_H_
