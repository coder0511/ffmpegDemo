/*----------------------------------------------------------------------------------------------
*
* This file is ArcSoft's property. It contains ArcSoft's trade secret, proprietary and 		
* confidential information. 
* 
* The information and code contained in this file is only for authorized ArcSoft employees 
* to design, create, modify, or review.
* 
* DO NOT DISTRIBUTE, DO NOT DUPLICATE OR TRANSMIT IN ANY FORM WITHOUT PROPER AUTHORIZATION.
* 
* If you are not an intended recipient of this file, you must not copy, distribute, modify, 
* or take any action in reliance on it. 
* 
* If you have received this file in error, please immediately notify ArcSoft and 
* permanently delete the original and any copy of any file and any printout thereof.
*
*-------------------------------------------------------------------------------------------------*/
 
/*
 * G726Decoder.h
 *
 * Description:
 *
 *	The common used marco and structure  in MVLIB2.0
 *
 *
 * History
 *    
 *  02-27-2014 Haiyan Huang  
 * - initial version 
 *
 *
 */

#ifndef _G726_DECODER_LINUX_H_
#define _G726_DECODER_LINUX_H_

//#include "G726.h"

#define GLOBAL __attribute__((visibility("default")))
#define LOCAL  __attribute__((visibility("hidden")))

#define MV2_ERR_NONE    0x0000
#define MV2_ERR_INVALID_PARAM -1

GLOBAL void* createG726Dec();

GLOBAL void  deleteG726Dec(void* hPP);

/*	decodeFrame 
 * 
 *	Description: 
 *		Call this function to decode one encoded media data frame. 
 *		It is a memory buffer to memory buffer operation. 
 *
 *	Parameters: 
 *		pIn : 			[in] 			Pointer to the buffer containing the media data to be decoded.
 *		lInSize: 		       [in] 			Number of bytes to decode
 *		plInSize		       [out]	       pointer to bytes that have been deocded of the pIn buffer
 *		pOut: 			[out] 		Pointer to the buffer that receives the data decode from the input buffer.
 *		lOutBufferSize:      [in] 			The byte size of the output buffer pointed by pOut. 
 *		plOutSize:		[out] 		Pointer to the number of bytes raw data that decodeframe() filled in the output buff. 
 *	
 *
 *	Return:
 *		MV2_ERR_NONE			success
 *		!MV2_ERR_NONE			fail
 */
GLOBAL long decodeG726Frame(void* hPP, unsigned char* pIn, long lInSize, long * plInSize, 
							  short* pOut, long lOutBufferSize, long * plOutSize);


GLOBAL long G726ResamplePcm(void* hPP, unsigned long srcSampleRate, 
				           unsigned long dstSampleRate, 
                           unsigned char* pdstPcmBuf,
				           long ldstPcmBufSize,
				           unsigned char* psrcPcmBuf,
				           long lsrcPcmBufSize );


#endif // _G726_DECODER_LINUX_H_
