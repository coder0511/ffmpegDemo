﻿/**
 * 最简单的基于FFmpeg的视频编码器
 * Simplest FFmpeg Video Encoder
 * 
 * 田野 tianye
 * 15158801467@163.com
 * 
 * 本程序实现了YUV像素数据编码为视频码流（H264，MPEG2，VP8等等）。
 * 是最简单的FFmpeg视频编码方面的教程。
 * 通过学习本例子可以了解FFmpeg的编码流程。
 * This software encode YUV420P data to H.264 bitstream.
 * It's the simplest video encoding software based on FFmpeg. 
 * Suitable for beginner of FFmpeg 
 */

#include <stdio.h>

#define __STDC_CONSTANT_MACROS

#ifdef _WIN32
//Windows
extern "C"
{
#include "libavutil/opt.h"
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
};
#else
//Linux...
#ifdef __cplusplus
extern "C"
{
#endif
#include <libavutil/opt.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#ifdef __cplusplus
};
#endif
#endif


int flush_encoder(AVFormatContext *fmt_ctx,unsigned int stream_index){
	int ret;
	int got_frame;
	AVPacket enc_pkt;
	if (!(fmt_ctx->streams[stream_index]->codec->codec->capabilities &
		CODEC_CAP_DELAY))
		return 0;
	while (1) {
		enc_pkt.data = NULL;
		enc_pkt.size = 0;
		av_init_packet(&enc_pkt);
		ret = avcodec_encode_video2 (fmt_ctx->streams[stream_index]->codec, &enc_pkt,
			NULL, &got_frame);
		av_frame_free(NULL);
		if (ret < 0)
			break;
		if (!got_frame){
			ret=0;
			break;
		}
		printf("Flush Encoder: Succeed to encode 1 frame!\tsize:%5d\n",enc_pkt.size);
		/* mux encoded frame */
		ret = av_write_frame(fmt_ctx, &enc_pkt);
		if (ret < 0)
			break;
	}
	return ret;
}

int main(int argc, char* argv[])
{
	AVFormatContext* pFormatCtx;
	AVOutputFormat* fmt;
	AVStream* video_st;
	AVCodecContext* pCodecCtx;
	AVCodec* pCodec;
	AVPacket pkt;
	uint8_t* picture_buf;
	AVFrame* pFrame;
	int picture_size;
	int y_size;
	int framecnt=0;
	//FILE *in_file = fopen("src01_480x272.yuv", "rb");	//Input raw YUV data 
	FILE *in_file = fopen("ds_480x272.yuv", "rb");       //Input raw YUV data
	int in_w=480,in_h=272;                              //Input data's width and height
	int framenum=100;                                   //Frames to encode
	//const char* out_file = "src01.h264";              //Output Filepath 
	//const char* out_file = "ds.ts";
	//const char* out_file = "ds.hevc";
	const char* out_file = "ds.h264";

    //FFmpeg接口第一个调用接口，完成大量初始化事情
	av_register_all();
    
	//Method1,其实用avformat_alloc_output_context2接口就可以完成下面这两件事
	//申请内存
	pFormatCtx = avformat_alloc_context();
	//Guess Format，根据文件后缀寻找匹配的AVOutputFormat
	fmt = av_guess_format(NULL, out_file, NULL);
	pFormatCtx->oformat = fmt;
	
	//Method 2.实质也就调用了avformat_alloc_context和av_guess_format接口
	//avformat_alloc_output_context2(&pFormatCtx, NULL, NULL, out_file);
	//fmt = pFormatCtx->oformat;


	//Open output URL,根据输出文件后缀打开相应的文件，同时支持协议
	if (avio_open(&pFormatCtx->pb,out_file, AVIO_FLAG_READ_WRITE) < 0){
		printf("Failed to open output file! \n");
		return -1;
	}

    //创建输出流
	video_st = avformat_new_stream(pFormatCtx, 0);
	//video_st->time_base.num = 1; 
	//video_st->time_base.den = 25;  

	if (video_st==NULL){
		return -1;
	}
	//Param that must set，配置编码器上下文的参数
	pCodecCtx = video_st->codec;
	//pCodecCtx->codec_id =AV_CODEC_ID_HEVC;
	pCodecCtx->codec_id = fmt->video_codec;          //指定视频编码器类型是H264还是H265
	pCodecCtx->codec_type = AVMEDIA_TYPE_VIDEO;     //指定编码器类型是视频编码器
	pCodecCtx->pix_fmt = AV_PIX_FMT_YUV420P;        //原始数据的像素格式，此处采用YUV420形式
	pCodecCtx->width = in_w;                        //代表视频的宽
	pCodecCtx->height = in_h;                       //代表视频的高
	pCodecCtx->bit_rate = 400000;                   //平均比特率即大概400kb/s
	pCodecCtx->gop_size = 50;                        //每250帧插入1个I帧，I帧越少，视频越小,也就是I帧间隔

	pCodecCtx->time_base.num = 1;                   //指定时间基，可以根据该参数将PTS转化为秒
	pCodecCtx->time_base.den = 25;                  //编码层时间基一般以帧率来指定

	//H264,高级选项设置
	//pCodecCtx->me_range = 16;
	//pCodecCtx->max_qdiff = 4;                     //量化标度间最大偏差 
	//pCodecCtx->qcompress = 0.6;
	pCodecCtx->qmin = 10;                           //最大和最小量化系数
	pCodecCtx->qmax = 51;

	//Optional Param                                //两个非B帧之间允许出现多少个B帧数据，如果为0则表示没有B帧
	pCodecCtx->max_b_frames=3;

	// Set Option
	AVDictionary *param = 0;
	//H.264 用264的API设置编码速度
	if(pCodecCtx->codec_id == AV_CODEC_ID_H264) {
		av_dict_set(&param, "preset", "slow", 0);
		av_dict_set(&param, "tune", "zerolatency", 0);
		//av_dict_set(&param, "profile", "main", 0);
	}
	//H.265
	if(pCodecCtx->codec_id == AV_CODEC_ID_H265){
		av_dict_set(&param, "preset", "ultrafast", 0);
		av_dict_set(&param, "tune", "zero-latency", 0);
	}

	//Show some Information
	av_dump_format(pFormatCtx, 0, out_file, 1);

    //寻找合适的编码器
	pCodec = avcodec_find_encoder(pCodecCtx->codec_id);
	if (!pCodec){
		printf("Can not find encoder! \n");
		return -1;
	}
	if (avcodec_open2(pCodecCtx, pCodec,&param) < 0){
		printf("Failed to open encoder! \n");
		return -1;
	}

    //申请解码后一帧数据的内存即原始的YUV数据
	pFrame = av_frame_alloc();
	picture_size = avpicture_get_size(pCodecCtx->pix_fmt, pCodecCtx->width, pCodecCtx->height);
	picture_buf = (uint8_t *)av_malloc(picture_size);
	avpicture_fill((AVPicture *)pFrame, picture_buf, pCodecCtx->pix_fmt, pCodecCtx->width, pCodecCtx->height);

	//Write File Header，写数据头信息
	avformat_write_header(pFormatCtx,NULL);

    //这里存储编码胡的一帧数据
	av_new_packet(&pkt,picture_size);

    //计算图像的分辨率
	y_size = pCodecCtx->width * pCodecCtx->height;

    //循环编码
	for (int i=0; i<framenum; i++){
		//Read raw YUV data
		if (fread(picture_buf, 1, y_size*3/2, in_file) <= 0){
			printf("Failed to read raw data! \n");
			return -1;
		}else if(feof(in_file)){
			break;
		}
        //这里和YUV的打包方式有密切关系
		pFrame->data[0] = picture_buf;              // Y
		pFrame->data[1] = picture_buf+ y_size;      // U 
		pFrame->data[2] = picture_buf+ y_size*5/4;  // V
		//PTS
		//pFrame->pts=i;， i表示当前编码的帧数，计算PTS
		long lPts = i*(video_st->time_base.den)/((video_st->time_base.num)*25);
		pFrame->pts= lPts;
		int got_picture=0;
		//Encode
		int ret = avcodec_encode_video2(pCodecCtx, &pkt,pFrame, &got_picture);
		if(ret < 0){
			printf("Failed to encode! \n");
			return -1;
		}
		if (got_picture==1){
			printf("Succeed to encode frame: %5d\tsize:%5d pts:%ld\n", framecnt, pkt.size, lPts);
			framecnt++;
			pkt.stream_index = video_st->index;
            //将编码的pkt一帧帧数据写到输出文件中
			ret = av_write_frame(pFormatCtx, &pkt);
			av_free_packet(&pkt);
		}
	}
	//Flush Encoder，由于存在B帧测存在编码延时，所以将所有数据编码完还需要调用该接口将剩余的编码帧输出到文件
	//如果没有B帧则可以认为没有延时
	int ret = flush_encoder(pFormatCtx,0);
	if (ret < 0) {
		printf("Flushing encoder failed\n");
		return -1;
	}

	//Write file trailer，收尾
	av_write_trailer(pFormatCtx);

	//Clean
	if (video_st){
		avcodec_close(video_st->codec);
		av_free(pFrame);
		av_free(picture_buf);
	}
	avio_close(pFormatCtx->pb);
	avformat_free_context(pFormatCtx);

	fclose(in_file);

	return 0;
}

