/**
 * 最简单的基于FFmpeg的视音频复用器
 * Simplest FFmpeg Muxer
 *
 * 田野 tianye
 * 15158801467@163.com
 * 本程序可以将视频码流和音频码流打包到一种封装格式中。
 * 程序中将AAC编码的音频码流和H.264编码的视频码流打包成
 * MPEG2TS封装格式的文件。
 * 需要注意的是本程序并不改变视音频的编码格式。
 *
 * This software mux a video bitstream and a audio bitstream 
 * together into a file.
 * In this example, it mux a H.264 bitstream (in MPEG2TS) and 
 * a AAC bitstream file together into MP4 format file.
 *
 */

#include <stdio.h>

#define __STDC_CONSTANT_MACROS

#ifdef _WIN32
//Windows
extern "C"
{
#include "libavformat/avformat.h"
};
#else
//Linux...
#ifdef __cplusplus
extern "C"
{
#endif
#include <libavformat/avformat.h>
#ifdef __cplusplus
};
#endif
#endif

/*
FIX: H.264 in some container format (FLV, MP4, MKV etc.) need 
"h264_mp4toannexb" bitstream filter (BSF)
  *Add SPS,PPS in front of IDR frame
  *Add start code ("0,0,0,1") in front of NALU
H.264 in some container (MPEG2TS) don't need this BSF.
*/
//'1': Use H.264 Bitstream Filter 
#define USE_H264BSF 0

/*
FIX:AAC in some container format (FLV, MP4, MKV etc.) need 
"aac_adtstoasc" bitstream filter (BSF)
*/
//'1': Use AAC Bitstream Filter 
#define USE_AACBSF 0



int main(int argc, char* argv[])
{
	AVOutputFormat *ofmt = NULL;            //存储输出视音频封装格式信息结构体
	//Input AVFormatContext and Output AVFormatContext
	//存储输入视音频封装格式视频信息和输出视音频封装格式信息结构体
	AVFormatContext *ifmt_ctx_v = NULL, *ifmt_ctx_a = NULL,*ofmt_ctx = NULL;
    AVPacket pkt;                           //存储压缩编码数据相关信息的结构体
	int ret, i;
	int videoindex_v=-1,videoindex_out=-1;
	int audioindex_a=-1,audioindex_out=-1;
	int frame_index=0;
	int64_t cur_pts_v=0,cur_pts_a=0;

    //输入的音频和视频文件 //Input file URL
	//const char *in_filename_v = "cuc_ieschool.ts";
	const char *in_filename_v = "cuc_ieschool.h264";
	//const char *in_filename_a = "cuc_ieschool.mp3";
	//const char *in_filename_a = "gowest.m4a";
	//const char *in_filename_a = "gowest.aac";
	const char *in_filename_a = "huoyuanjia.mp3";

    //目的封装输出文件 //Output file URL
	const char *out_filename = "cuc_ieschool.mp4";

    //这个完成了复用器和解复用器 编解码 网络协议注册器，是所有接口调用的第一个接口
    av_register_all();

    //打开多媒体数据，获取相关的媒体信息，给AVFormatContext分配内存
        //里面调用了init_input函数打开输入的视频数据并且探测视频的格式
        //读取多媒体数据文件头，根据视音频流创建相应的AVStream
	if ((ret = avformat_open_input(&ifmt_ctx_v, in_filename_v, 0, 0)) < 0) {
		printf( "Could not open input file.");
		goto end;
	}

    //该函数可以读取一部分视音频数据并且获得一些相关的信息，主要用于给每个媒体流（音频/视频）的AVStream结构体赋值
    /*
        1.查找解码器：find_decoder()
        2.打开解码器：avcodec_open2()
        3.读取完整的一帧压缩编码的数据：read_frame_internal()
        注：av_read_frame()内部实际上就是调用的read_frame_internal()
        4.解码一些压缩编码数据：try_decode_frame()
    */
    if ((ret = avformat_find_stream_info(ifmt_ctx_v, 0)) < 0) {
		printf( "Failed to retrieve input stream information");
		goto end;
	}

    //音频同上视频
	if ((ret = avformat_open_input(&ifmt_ctx_a, in_filename_a, 0, 0)) < 0) {
		printf( "Could not open input file.");
		goto end;
	}
	if ((ret = avformat_find_stream_info(ifmt_ctx_a, 0)) < 0) {
		printf( "Failed to retrieve input stream information");
		goto end;
	}

    //打印读出MetaData的信息,其中包含时长 帧率 采样率 通道个数等信息
	printf("===========input AVFormatContext Video MetaData Information==========\n");
	av_dump_format(ifmt_ctx_v, 0, in_filename_v, 0);
	printf("=====================================================================\n");

    printf("===========input AVFormatContext Audio MetaData Information==========\n");
    av_dump_format(ifmt_ctx_a, 0, in_filename_a, 0);
	printf("======================================================================\n");

    //初始化一个用于输出的AVFormatContext结构体，一般在编码系统中调用的函数
        //avformat_alloc_context()初始化一个默认的AVFormatContext。
        //如果没有指定AVOutputFormat即第二个参数，则根据最后两个参数用av_guess_format()最终确认AVFormatContext
	avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, out_filename);
	if (!ofmt_ctx) {
		printf( "Could not create output context\n");
		ret = AVERROR_UNKNOWN;
		goto end;
	}
	ofmt = ofmt_ctx->oformat;

    //给输出音视频的AVFormatContext创建视频Stream 通道，由于这里是转封装并不改变音视频编码格式，
    //所以可以复制相关参数信息,这一步比较灵活，一般情况都是自己要指定相关参数再生成，复制的较少
	for (i = 0; i < ifmt_ctx_v->nb_streams; i++) {
		//Create output AVStream according to input AVStream
		if(ifmt_ctx_v->streams[i]->codec->codec_type==AVMEDIA_TYPE_VIDEO){
		AVStream *in_stream = ifmt_ctx_v->streams[i];
		AVStream *out_stream = avformat_new_stream(ofmt_ctx, in_stream->codec->codec);
		videoindex_v=i;
		if (!out_stream) {
			printf( "Failed allocating output stream\n");
			ret = AVERROR_UNKNOWN;
			goto end;
		}
		videoindex_out=out_stream->index;
		//Copy the settings of AVCodecContext
		if (avcodec_copy_context(out_stream->codec, in_stream->codec) < 0) {
			printf( "Failed to copy context from input to output stream codec context\n");
			goto end;
		}
		out_stream->codec->codec_tag = 0;
		if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
			out_stream->codec->flags |= CODEC_FLAG_GLOBAL_HEADER;
		break;
		}
	}

    //在输出的AVFormatContext中创建音频的Stream通道
	for (i = 0; i < ifmt_ctx_a->nb_streams; i++) {
		//Create output AVStream according to input AVStream
		if(ifmt_ctx_a->streams[i]->codec->codec_type==AVMEDIA_TYPE_AUDIO){
			AVStream *in_stream = ifmt_ctx_a->streams[i];
			AVStream *out_stream = avformat_new_stream(ofmt_ctx, in_stream->codec->codec);
			audioindex_a=i;
			if (!out_stream) {
				printf( "Failed allocating output stream\n");
				ret = AVERROR_UNKNOWN;
				goto end;
			}
			audioindex_out=out_stream->index;
			//Copy the settings of AVCodecContext
			if (avcodec_copy_context(out_stream->codec, in_stream->codec) < 0) {
				printf( "Failed to copy context from input to output stream codec context\n");
				goto end;
			}
			out_stream->codec->codec_tag = 0;
			if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
				out_stream->codec->flags |= CODEC_FLAG_GLOBAL_HEADER;

			break;
		}
	}
    

	printf("==========Output AVFormatContext Video And Audio MetaData Information==========\n");
	av_dump_format(ofmt_ctx, 0, out_filename, 1);
	printf("================================================================================\n");

    //该函数用于打开FFmpeg的输入输出文件,为写相应的文件做准备
        //其中传递最重要的两个参数一个是函数调用成功之后创建的AVIOContext结构体
        //输入输出协议的地址（文件也是一种“广义”的协议，对于文件来说就是文件的路径）
    /*
    主要调用了2个函数：ffurl_open()和ffio_fdopen()
    其中ffurl_open()用于初始化URLContext,对于文件就是打开文件句柄，对于rtmp就是建立连接等
    ffio_fdopen()用于根据URLContext初始化AVIOContext，为后续的读写数据做准备，申请内存等操作
    URLContext中包含的URLProtocol完成了具体的协议读写等工作。
    AVIOContext则是在URLContext的读写函数外面加上了一层“包装
    */
	if (!(ofmt->flags & AVFMT_NOFILE)) {
		if (avio_open(&ofmt_ctx->pb, out_filename, AVIO_FLAG_WRITE) < 0) {
			printf( "Could not open output file '%s'", out_filename);
			goto end;
		}
	}
	//写文件头，正式写流的第一步，总共包含三个函数avformat_write_header(),av_write_frame()以及av_write_trailer()
	//之所以此时能写头，是因为基本在前面对ofmt_ctx的操作已经具备了写码流头的条件
	/*
    （1）调用init_muxer()初始化复用器，做各种检查，检查AVFormatContext输出AVStream的时间基，检查音频的采样率，视频的宽高等信息
    （2）调用AVOutputFormat的write_header()，要对应到实际的Context去写文件头
	*/
	if (avformat_write_header(ofmt_ctx, NULL) < 0) {
		printf( "Error occurred when opening output file\n");
		goto end;
	}


	//FIX
#if USE_H264BSF
	AVBitStreamFilterContext* h264bsfc =  av_bitstream_filter_init("h264_mp4toannexb"); 
#endif
#if USE_AACBSF
	AVBitStreamFilterContext* aacbsfc =  av_bitstream_filter_init("aac_adtstoasc"); 
#endif

	while (1) {
		AVFormatContext *ifmt_ctx;
		int stream_index=0;
		AVStream *in_stream, *out_stream;

		//音视频流复用（MUX）的时候比较时间戳，根据时间戳的先后顺序交叉写入音频包或视频包
		if(av_compare_ts(cur_pts_v,ifmt_ctx_v->streams[videoindex_v]->time_base,cur_pts_a,ifmt_ctx_a->streams[audioindex_a]->time_base) <= 0){
			ifmt_ctx=ifmt_ctx_v;
			stream_index=videoindex_out;

			if(av_read_frame(ifmt_ctx, &pkt) >= 0){
				do{
					in_stream  = ifmt_ctx->streams[pkt.stream_index];
					out_stream = ofmt_ctx->streams[stream_index];

					if(pkt.stream_index==videoindex_v){
						//FIX：No PTS (Example: Raw H.264)
						//Simple Write PTS
						//如果视频帧没有PTS，则我们需要自己根据帧率生成PTS
						if(pkt.pts==AV_NOPTS_VALUE){
							//Write PTS
							AVRational time_base1=in_stream->time_base;
							//Duration between 2 frames (us),也就是相邻两帧的时间间隔，真实时间值
							//av_q2d(in_stream->r_frame_rate)代表帧率的倒数，也就是真实的时间，单位秒
							int64_t calc_duration=(double)AV_TIME_BASE/av_q2d(in_stream->r_frame_rate);
							//注意这里的计算方式，用帧数*每帧的持续真实时间，就是真实的秒数，然后再换算出时间刻度，这样
							//就计算出来了当前帧的PTS即用了多少时间刻度，时间刻度就是把1秒划分的格子数
							//只适合没有B帧的计算方式
							pkt.pts=(double)(frame_index*calc_duration)/(double)(av_q2d(time_base1)*AV_TIME_BASE);
							pkt.dts=pkt.pts;
							pkt.duration=(double)calc_duration/(double)(av_q2d(time_base1)*AV_TIME_BASE);
							frame_index++;
						}

						cur_pts_v=pkt.pts;
						break;
					}
				}while(av_read_frame(ifmt_ctx, &pkt) >= 0);
			}else{
				break;
			}
		}else{
			ifmt_ctx=ifmt_ctx_a;
			stream_index=audioindex_out;
			if(av_read_frame(ifmt_ctx, &pkt) >= 0){
				do{
					in_stream  = ifmt_ctx->streams[pkt.stream_index];
					out_stream = ofmt_ctx->streams[stream_index];

					if(pkt.stream_index==audioindex_a){

						//FIX：No PTS
						//Simple Write PTS
						if(pkt.pts==AV_NOPTS_VALUE){
							//Write PTS
							AVRational time_base1=in_stream->time_base;
							//Duration between 2 frames (us)
							int64_t calc_duration=(double)AV_TIME_BASE/av_q2d(in_stream->r_frame_rate);
							//Parameters
							pkt.pts=(double)(frame_index*calc_duration)/(double)(av_q2d(time_base1)*AV_TIME_BASE);
							pkt.dts=pkt.pts;
							pkt.duration=(double)calc_duration/(double)(av_q2d(time_base1)*AV_TIME_BASE);
							frame_index++;
						}
						cur_pts_a=pkt.pts;

						break;
					}
				}while(av_read_frame(ifmt_ctx, &pkt) >= 0);
			}else{
				break;
			}

		}

		//FIX:Bitstream Filter
#if USE_H264BSF
		av_bitstream_filter_filter(h264bsfc, in_stream->codec, NULL, &pkt.data, &pkt.size, pkt.data, pkt.size, 0);
#endif
#if USE_AACBSF
		av_bitstream_filter_filter(aacbsfc, out_stream->codec, NULL, &pkt.data, &pkt.size, pkt.data, pkt.size, 0);
#endif


		//Convert PTS/DTS，转换成输出视频流的对应参数信息
		pkt.pts = av_rescale_q_rnd(pkt.pts, in_stream->time_base, out_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
		pkt.dts = av_rescale_q_rnd(pkt.dts, in_stream->time_base, out_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
		pkt.duration = av_rescale_q(pkt.duration, in_stream->time_base, out_stream->time_base);
		pkt.pos = -1;
		pkt.stream_index=stream_index;

		printf("Write 1 Packet. size:%5d\tpts:%lld\n",pkt.size,pkt.pts);
		//Write，写数据
		if (av_interleaved_write_frame(ofmt_ctx, &pkt) < 0) {
			printf( "Error muxing packet\n");
			break;
		}
        //每写一帧音视频，都要释放该pkt,否则容易造成内存泄漏
		av_free_packet(&pkt);

	}
	//Write file trailer，写入文件尾
	av_write_trailer(ofmt_ctx);

#if USE_H264BSF
	av_bitstream_filter_close(h264bsfc);
#endif
#if USE_AACBSF
	av_bitstream_filter_close(aacbsfc);
#endif

end:
	avformat_close_input(&ifmt_ctx_v);
	avformat_close_input(&ifmt_ctx_a);
	/* close output */
	if (ofmt_ctx && !(ofmt->flags & AVFMT_NOFILE))
		avio_close(ofmt_ctx->pb);
	avformat_free_context(ofmt_ctx);
	if (ret < 0 && ret != AVERROR_EOF) {
		printf( "Error occurred.\n");
		return -1;
	}
	return 0;
}


